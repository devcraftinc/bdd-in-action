package example;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ParkingRepository extends CrudRepository<ParkingRepository.ParkingEntry, Long> {

	@Entity(name = "parking_entry")
	public class ParkingEntry {

		@Id
		@GeneratedValue
		private long code;
		private long time;

		public long getCode() {
			return code;
		}

		public long getTime() {
			return time;
		}

		public void setTime(long time) {
			this.time = time;
		}
	}

}