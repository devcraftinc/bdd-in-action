package example;

import static java.time.Duration.ofHours;
import static java.time.Duration.ofMinutes;

public class ParkingService {

	private static final int INTERVAL_RATE = 3;
	private static final int FIRST_HOUR_RATE = 10;
	private final Clock clock;
	private final ParkingStorage storage;

	public ParkingService(Clock clock, ParkingStorage storage) {
		this.clock = clock;
		this.storage = storage;
	}

	public int calcPayment(long code) {
		long entryTime = storage.fetchEntryTime(code);
		long paymentTime = clock.now();
		return calcPayment(entryTime, paymentTime);
	}

	private int calcPayment(long entryTime, long paymentTime) {
		long timeInParking = paymentTime - entryTime;
		if (timeInParking < ofMinutes(10).toMillis()) {
			return 0;
		}
		if (timeInParking < ofHours(1).toMillis()) {
			return ParkingService.FIRST_HOUR_RATE;
		}

		long extraTime = timeInParking - ofHours(1).toMillis();
		int numOfIntervals = intervalsIn(extraTime, 15);
		return ParkingService.FIRST_HOUR_RATE + ParkingService.INTERVAL_RATE * numOfIntervals;
	}

	public long enterParking() {
		long entryTime = clock.now();
		return storage.addParkingEntry(entryTime);
	}

	private int intervalsIn(long extraTime, int intervalInMin) {
		return 1 + (int) (extraTime / ofMinutes(intervalInMin).toMillis());
	}

}