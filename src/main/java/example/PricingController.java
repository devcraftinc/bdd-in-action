package example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class PricingController {

	private final ParkingService parkingService;

	public PricingController(ParkingService parkingService) {
		this.parkingService = parkingService;
	}

	@GetMapping("/parking/{code}/amount")
	public int calcPayment(@PathVariable long code) {
		return parkingService.calcPayment(code);
	}

	@PostMapping("/parking")
	public long enterParking() {
		return parkingService.enterParking();
	}

}