package example;

import org.springframework.stereotype.Component;

import example.ParkingRepository.ParkingEntry;

@Component
class JpaParkingStorage implements ParkingStorage {
	private ParkingRepository repository;

	public JpaParkingStorage(ParkingRepository repository) {
		this.repository = repository;
	}

	@Override
	public long addParkingEntry(long entryTime) {
		ParkingEntry entity = new ParkingEntry();
		entity.setTime(entryTime);
		return repository.save(entity).getCode();
	}

	@Override
	public long fetchEntryTime(long code) {
		return repository.findById(code).get().getTime();
	}
}