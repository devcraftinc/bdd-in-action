package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	Clock clock() {
		return System::currentTimeMillis;
	}

	@Bean
	ParkingService parkingService(Clock clock, ParkingStorage storage) {
		return new ParkingService(clock, storage);
	}

	@Bean
	PricingController pricingController(ParkingService service) {
		return new PricingController(service);
	}
}
