Feature: pricing

  Scenario Outline: hourly payment
    Given I entered the parking at "<entry time>"
    When I pay at "<payment time>"
    Then I should pay <expected payment>

    Examples: 
      | entry time | payment time | expected payment |
      | 10:00:00   | 10:09:59     |                0 |
      | 10:00:00   | 10:10:00     |               10 |
      | 10:00:00   | 10:59:59     |               10 |
      | 10:00:00   | 11:00:00     |               13 |
      | 10:00:00   | 11:15:00     |               16 |
