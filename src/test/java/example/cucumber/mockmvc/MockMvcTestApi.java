package example.cucumber.mockmvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.io.UnsupportedEncodingException;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import example.cucumber.steps.TestApi;

class MockMvcTestApi implements TestApi {

	ClockMock clock;
	MockMvc httpClient;

	public MockMvcTestApi(ClockMock clock, MockMvc httpClient) {
		super();
		this.clock = clock;
		this.httpClient = httpClient;
	}

	@Override
	public void assumeTimeIs(long time) {
		clock.setCurrTime(time);
	}

	@Override
	public int calcPayment(long codeOnCard) throws Exception {
		return Integer.parseInt(httpRequest(get("/parking/" + codeOnCard + "/amount")));
	}

	@Override
	public long enterParking() throws Exception {
		return Long.parseLong(httpRequest(post("/parking")));
	}

	private String httpRequest(MockHttpServletRequestBuilder action) throws UnsupportedEncodingException, Exception {
		return httpClient.perform(action.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
	}
}