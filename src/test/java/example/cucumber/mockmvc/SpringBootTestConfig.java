package example.cucumber.mockmvc;

import java.text.ParseException;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;

import example.Clock;
import example.cucumber.steps.TestApi;
import io.cucumber.java.en.Given;

/**
 * @author Eran
 * 
 *         This is the tricky part. cucumber-spring will find any steps class
 *         with @SpringBootTest and use it as a SpringBootTest. We must have
 *         some "never-invoked" step for cucumber to understand this is a steps
 *         class.
 */
@SpringBootTest(properties = { "spring.main.allow-bean-definition-overriding=true" })
@AutoConfigureMockMvc
public class SpringBootTestConfig {

	/**
	 * Override application beans and define test beans here.
	 */
	@TestConfiguration
	public static class TestConfig {

		@Bean
		Clock clock() {
			return new ClockMock();
		}

		@Bean
		TestApi testApi(ClockMock clock, MockMvc mockMvc) {
			return new MockMvcTestApi(clock, mockMvc);
		}
	}

	@Given("never invoked")
	public void neverInvoked() throws ParseException {
	}

}