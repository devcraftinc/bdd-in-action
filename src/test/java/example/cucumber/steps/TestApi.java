package example.cucumber.steps;

public interface TestApi {

	void assumeTimeIs(long time);

	int calcPayment(long codeOnCard) throws Exception;

	long enterParking() throws Exception;

}