package example.cucumber.steps;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Steps {

	private final TestApi testApi;

	int actualAmount;
	private long codeOnCard;

	public Steps(TestApi testApi) {
		this.testApi = testApi;
	}

	@Given("I entered the parking at {string}")
	public void i_entered_the_parking_at(String timeStr) throws Exception {
		testApi.assumeTimeIs(parseTime(timeStr));
		codeOnCard = testApi.enterParking();
	}

	@When("I pay at {string}")
	public void i_pay_at(String timeStr) throws Exception {
		testApi.assumeTimeIs(parseTime(timeStr));
		actualAmount = testApi.calcPayment(codeOnCard);
	}

	@Then("I should pay {int}")
	public void i_should_pay(Integer expectedAmount) {
		assertThat(actualAmount, equalTo(expectedAmount));
	}

	private long parseTime(String timeStr) throws ParseException {
		return new SimpleDateFormat("hh:mm:ss").parse(timeStr).getTime();
	}
}