package example.cucumber.noweb;

import example.ParkingService;
import example.cucumber.steps.TestApi;

class NoWebTestApi implements TestApi {

	private final ClockMock clock;
	private final ParkingService service;

	public NoWebTestApi(ClockMock clock, ParkingService service) {
		this.clock = clock;
		this.service = service;
	}

	@Override
	public void assumeTimeIs(long time) {
		clock.setCurrTime(time);
	}

	@Override
	public int calcPayment(long codeOnCard) throws Exception {
		return service.calcPayment(codeOnCard);
	}

	@Override
	public long enterParking() throws Exception {
		return service.enterParking();
	}
}