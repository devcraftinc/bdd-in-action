package example.cucumber.noweb;

import java.text.ParseException;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import example.Clock;
import example.ParkingService;
import example.cucumber.steps.TestApi;
import io.cucumber.java.en.Given;

/**
 * @author Eran
 * 
 *         This is the tricky part. cucumber-spring will find any steps class
 *         with @SpringBootTest and use it as a SpringBootTest. We must have
 *         some "never-invoked" step for cucumber to understand this is a steps
 *         class.
 */
@SpringBootTest(properties = { "spring.main.allow-bean-definition-overriding=true" })
public class SpringBootTestConfig {

	/**
	 * Override application beans and define test beans here.
	 */
	@TestConfiguration
	public static class TestConfig {

		@Bean
		Clock clock() {
			return new ClockMock();
		}

		@Bean
		TestApi testApi(ClockMock clock, ParkingService service) {
			return new NoWebTestApi(clock, service);
		}
	}

	@Given("never invoked")
	public void neverInvoked() throws ParseException {
	}

}